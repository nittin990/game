#include "DemoScene.h"

DemoScene::DemoScene()
{
    LoadContent();
}

void DemoScene::LoadContent()
{
    //set mau backcolor cho scene o day la mau xanh
    mBackColor = 0x54acd2;
	mAnimation = new Animation("Resources/captain-sprite.png",15,4,11,0.15);
	mAnimation->SetPosition(GameGlobal::GetWidth() / 5, GameGlobal::GetHeight() / 1.3);
	mAnimation->SetScale(D3DXVECTOR2(1, 1));
    mMap = new GameMap("Resources/map1.tmx");
	mCamera = new Camera(GameGlobal::GetWidth(), GameGlobal::GetHeight());
	mCamera->SetPosition(GameGlobal::GetWidth() / 2,
		mMap->GetHeight() - GameGlobal::GetHeight() / 2);
	mMap->SetCamera(mCamera);
}

void DemoScene::Update(float dt)
{
	mAnimation->Update(dt);
}

void DemoScene::Draw()
{
    mMap->Draw();
	mAnimation->Draw();
}

void DemoScene::OnKeyDown(int keyCode)
{
	if (keyCode == VK_LEFT)
	{
		mCamera->SetPosition(mCamera->GetPosition() + D3DXVECTOR3(-10, 0, 0));
		mAnimation->SetPosition(mAnimation->GetPosition() + D3DXVECTOR3(-10, 0, 0));
	}

	if (keyCode == VK_RIGHT)
	{
		mCamera->SetPosition(mCamera->GetPosition() + D3DXVECTOR3(10, 0, 0));
		mAnimation->SetPosition(mAnimation->GetPosition() + D3DXVECTOR3(10, 0, 0));
	}

	if (keyCode == VK_UP)
	{
		mCamera->SetPosition(mCamera->GetPosition() + D3DXVECTOR3(0, -10, 0));
	}

	if (keyCode == VK_DOWN)
	{
		mCamera->SetPosition(mCamera->GetPosition() + D3DXVECTOR3(0, 10, 0));
	}
}

void DemoScene::OnKeyUp(int keyCode)
{

}

void DemoScene::OnMouseDown(float x, float y)
{
}
