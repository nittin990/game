#include "Global.h"

HINSTANCE Global::mHInstance = NULL;
HWND Global::mHwnd = NULL;
LPD3DXSPRITE Global::mSpriteHandler = NULL;
int Global::mWidth = 600;
int Global::mHeight = 600;
LPDIRECT3DDEVICE9 Global::mDevice = nullptr;
bool Global::isGameRunning = true;
IDirect3DSurface9* Global::backSurface = nullptr;

Global::Global()
{

}


Global::~Global()
{
}

void Global::SetCurrentDevice(LPDIRECT3DDEVICE9 device)
{
	mDevice = device;
}

LPDIRECT3DDEVICE9 Global::GetCurrentDevice()
{
	return mDevice;
}


HINSTANCE Global::GetCurrentHINSTACE()
{
	return mHInstance;
}

HWND Global::getCurrentHWND()
{
	return mHwnd;
}

void Global::SetCurrentHINSTACE(HINSTANCE hInstance)
{
	mHInstance = hInstance;
}

void Global::SetCurrentHWND(HWND hWnd)
{
	mHwnd = hWnd;
}

void Global::SetCurrentSpriteHandler(LPD3DXSPRITE spriteHandler)
{
	mSpriteHandler = spriteHandler;
}

LPD3DXSPRITE Global::GetCurrentSpriteHandler()
{
	return mSpriteHandler;
}

void Global::SetWidth(int width)
{
	mWidth = width;
}

int Global::GetWidth()
{
	return mWidth;
}

void Global::SetHeight(int height)
{
	mHeight = height;
}

int Global::GetHeight()
{
	return mHeight;
}