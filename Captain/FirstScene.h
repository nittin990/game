#pragma once

#include <math.h>
#include <vector>
#include <d3dx9.h>
#include <d3d9.h>

#include "Scene.h"
#include "Sprite.h"
#include "Animation.h"

class FirstScene : public Scene
{
public:
	FirstScene();

	//void Update(float dt);
	bool init() override;
	void virtual updateInput(float dt) override;
	void virtual update(float dt) override;
	void virtual draw(LPD3DXSPRITE spriteHandle) override;
	void virtual release() override;
	//void Draw();

	void OnKeyDown(int keyCode);
	void OnKeyUp(int keyCode);
	void OnMouseDown(float x, float y);

protected:
	Animation *mGoldBlock;
	float mTimeCounter;
};

