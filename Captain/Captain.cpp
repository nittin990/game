#include <Windows.h>
#include <iostream>
#include "Game.h"
#include "CaptainGame.h"

using namespace std;

#define APP_TITTLE L"Captain"

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpcmdLine, int nCmdShow)
{
	string text = "abc";
	wchar_t test[20];
	size_t outSize;
	mbstowcs_s(&outSize, test, text.c_str(), text.length());
	LPWSTR title = test;
	Game* myGame;
	myGame = new CaptainGame(hInstance, test);

	myGame->init();
	myGame->run();
	myGame->release();

	delete myGame;
}