#pragma once
#include "Sprite.h"
#include "ViewPort.h"
class Tile {
private:
	int _id;
	RECT _srcRect;
	Sprite *_refSprite; //TileSet
public:
	Tile(Sprite *refSprite, RECT srcRect, int id);
	~Tile();
	const int &getId() const;
	void draw(LPD3DXSPRITE spriteHandle, GVector2 position, Viewport *viewport = NULL);
};
