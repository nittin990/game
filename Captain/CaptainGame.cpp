﻿#include "CaptainGame.h"
#include "FirstScene.h"

CaptainGame::CaptainGame(HINSTANCE hInstance, LPWSTR title) : Game(hInstance, title, WINDOW_WIDTH, WINDOW_HEIGHT)
{

}

CaptainGame::~CaptainGame()
{
}

void CaptainGame::init()
{
	Game::init();
	SceneManager::getInstance()->addScene(new FirstScene());
	//SceneManager::getInstance()->addScene(new Stage3(30));
	//SceneManager::getInstance()->addScene(new PlayScene());
	//SceneManager::getInstance()->addScene(new IntroScene());

}

void CaptainGame::release()
{
	Game::release();

	// release game
	SceneManager::getInstance()->clearScenes();
}

void CaptainGame::updateInput(float deltatime)
{
	SceneManager::getInstance()->updateInput(deltatime);
}

void CaptainGame::update(float deltatime)
{
	SceneManager::getInstance()->update(deltatime);
}

void CaptainGame::draw()
{
	this->_spriteHandle->Begin(D3DXSPRITE_ALPHABLEND);
	SceneManager::getInstance()->draw(_spriteHandle);
	this->_spriteHandle->End();
}

void CaptainGame::loadResource()
{
	// Game::init đã gọi hàm này rồi nên không cần gọi lại CaptainGame::loadResource
	// load resource
	SpriteManager::getInstance()->loadResource(_spriteHandle);
	//SoundManager::getInstance()->loadSound(Game::hWindow->getWnd());

}
