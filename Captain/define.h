#pragma once
#include "Global.h"
enum eID {
	MAP1 = 1,
};

#define C_WHITE D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f)
#define COLOR_KEY D3DXCOLOR(1.0f, 0.0f, 1.0f, 1.0f)			
typedef D3DXVECTOR3 GVector3;
typedef D3DXVECTOR2 GVector2;
#define VECTOR2ZERO GVector2(0.0f, 0.0f)
#define VECTOR2ONE  GVector2(1.0f, 1.0f)
#define WINDOW_WIDTH 512
#define WINDOW_HEIGHT 448
#define ACTOR_SCENARIO [event_receiver(native)]